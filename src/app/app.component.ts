import { Component , ElementRef, ViewChild , ViewChildren, QueryList} from '@angular/core';
import { ChildComponent} from './child/child.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'interaction';

  @ViewChild('btnref') btnreference: ElementRef;
  // @ViewChild(ChildComponent) dateref: ChildComponent;
  @ViewChildren(ChildComponent) dateref: QueryList<ChildComponent>;
   
  ngAfterViewInit() {
    console.log(this.dateref.toArray());
    
  }
  Change(){
    console.log(this.btnreference.nativeElement.innerText = 'Techigai');
     
  }
}
